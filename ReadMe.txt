Created by Mohammad Akhlaghi on 8/27/2015
email: mohammad.sfu@gmail.com
linkedIn: http://www.linkedin.com/in/mohammadak


This App contains a fragment which holds a list for All Clio Matters. The app needs to be connected to the internet for the data to load up on first launch. After first contact with internet the data will be stored in permanent storage to be used offline. Permanent storage used is Shared Preferences, the ideal way to store structured data is on a database however due to lack of time for this MVP I decided to save the object string on Shared Preferences.

Two major 3rd libraries are being used in conjunction with each other, one is Volley for asynchronous thread safe queue based network connectivity and the other is GSON for parsing json into java objects.

TODO comments are placed in various spots to show what still requires more improvement.

This code is by no means a final shippable product but it will satisfy the need of the MVP for this test.

Please feel free to share with me any comments or suggestions that you may have.
