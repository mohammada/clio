package trialapp.goclio.com.matterlist;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import trialapp.goclio.com.R;
import trialapp.goclio.com.webservices.models.ListMatters;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 */

public class MatterListAdapter extends BaseAdapter {

    private ArrayList<String> mDisplayNum;
    private ArrayList<String> mCreatedTimeText;
    private ArrayList<String> mUpdatedTimeText;
    private ArrayList<Integer> mMatterId;

    public MatterListAdapter(ListMatters listMatters) {
        mMatterId = listMatters.MatterIdList;
        mDisplayNum = listMatters.DisplayNumList;
        mCreatedTimeText = listMatters.CreatedTimeTextList;
        mUpdatedTimeText = listMatters.UpdatedTimeTextList;
    }

    @Override
    public int getCount() {
        return mMatterId.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return mMatterId.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            rowView = View.inflate(parent.getContext(), R.layout.list_row_all_matters, null);
            TextViewHolder viewHolder = new TextViewHolder();

            viewHolder.displayNumberTxtView = (TextView) rowView.findViewById(R.id.row_item_display_num);
            viewHolder.dateCreatedTxtView = (TextView) rowView.findViewById(R.id.row_item_created_date);
            viewHolder.dateUpdatedTxtView = (TextView) rowView.findViewById(R.id.row_item_updated_date);

            rowView.setTag(viewHolder);
        }

        // Nullity has already been checked in the wrapper class
        TextViewHolder textViewHolder = (TextViewHolder) rowView.getTag();
        textViewHolder.displayNumberTxtView.setText(mDisplayNum.get(position));
        textViewHolder.dateCreatedTxtView.setText(mCreatedTimeText.get(position));
        textViewHolder.dateUpdatedTxtView.setText(mUpdatedTimeText.get(position));

        return rowView;
    }

    public class TextViewHolder {
        TextView displayNumberTxtView;
        TextView dateCreatedTxtView;
        TextView dateUpdatedTxtView;
    }
}
