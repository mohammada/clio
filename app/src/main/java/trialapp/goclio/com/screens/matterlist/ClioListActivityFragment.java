package trialapp.goclio.com.screens.matterlist;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import trialapp.goclio.com.R;
import trialapp.goclio.com.matterlist.MatterListAdapter;
import trialapp.goclio.com.utils.SharedPref;
import trialapp.goclio.com.utils.Track;
import trialapp.goclio.com.webservices.AllMattersListener;
import trialapp.goclio.com.webservices.NetworkManager;
import trialapp.goclio.com.webservices.models.Matters;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 */

public class ClioListActivityFragment extends Fragment implements AllMattersListener, AdapterView.OnItemClickListener {

    private static final String TAG = ClioListActivityFragment.class.getSimpleName();
    // Tag to be sent to the volley for control over the request queue
    private Object mTag = new Object();

    // This variable will be holding all data for the MVP, a CacheManager should be implemented
    // to better take care of this in an actual app
    private Matters mAllMatter;

    private MatterListAdapter mMatterListAdapter;
    private ListView mMattersList;
    private TextView mMainStatusBar;

    public ClioListActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.fragment_clio_list, container, false);

        mMattersList = (ListView) content.findViewById(R.id.all_matters_list);
        mMainStatusBar = (TextView) content.findViewById(R.id.main_status_bar);

        mMattersList.setOnItemClickListener(this);

        return content;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                loadAllMatterData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        loadAllMatterData();
    }

    // Try different methods both online and offline to load the data
    // show status messages to let user know what is going on
    private void loadAllMatterData() {
        if (isNetworkConnected()) {
            setStatusBar(getActivity().getString(R.string.status_message_data_loading_internet));
            NetworkManager.getInstance(getActivity()).getAllMattersFromServer(mTag, this);
        } else if (SharedPref.isDataStored(getActivity())) {
            setStatusBar(getActivity().getString(R.string.status_message_data_loading_file));
            String matterData = SharedPref.getMatterData(getActivity());
            if (!TextUtils.isEmpty(matterData)) {
                mAllMatter = new Gson().fromJson(matterData, Matters.class);
                populateListView();
            } else {
                // It should never get to here because data has to be loaded by now
                setStatusBar(getActivity().getString(R.string.status_message_data_file_not_present));
            }
        } else {
            setStatusBar(getActivity().getString(R.string.status_message_no_internet_no_data));
        }
    }

    // control status bar
    private void setStatusBar(String status) {
        mMainStatusBar.setVisibility(View.VISIBLE);
        mMainStatusBar.setText(status);
    }

    private void hideStatusBar() {
        mMainStatusBar.setVisibility(View.GONE);
    }

    // listener on Volley response
    @Override
    public void onMattersDownloadSuccess(Matters allMattersModel) {
        Track.i(TAG, "Volley Success");

        mAllMatter = allMattersModel;
        populateListView();

        // store the matter model as a text in sharedPref for offline use
        // todo do a proper database to store the data
        SharedPref.savePreferences(getActivity(), new Gson().toJson(mAllMatter));

        hideStatusBar();
    }

    private void populateListView() {
        if (mAllMatter != null) {
            // Populate the listview
            mMatterListAdapter = new MatterListAdapter(mAllMatter.getListMatters());
            mMattersList.setAdapter(mMatterListAdapter);
        }
        hideStatusBar();
    }


    @Override
    public void onMattersDownloadFailed(String errorMessage, int statusCode) {
        Track.e(TAG, "Volley failed: " + errorMessage + " Status Code: " + statusCode);
        hideStatusBar();
    }

    // Is internet available or not
    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Track.i(TAG, "Matter item " + position + " clicked");
        openIndividualProfile(position);
    }

    // todo input parameter has to be change to something a lot more reliable
    // for example using getItem from the list that returns a unique value (wasn't sure if id was unique)
    // and looking into matter data to search for that unique item details
    private void openIndividualProfile(int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder.setTitle(R.string.dialog_title);
        Matters.MattersSubclass matterSelected = mAllMatter.matters.get(position);
        String displayMessage;
        if (matterSelected != null) {
            String display_number = checkForNullity(matterSelected.display_number);
            String name = checkForNullity(matterSelected.client.name);
            String description = checkForNullity(matterSelected.description);
            String open_date = checkForNullity(matterSelected.open_date);
            String status = checkForNullity(matterSelected.status);
            displayMessage = String.format(getActivity().getString(R.string.dialog_message_for_client_profile),
                    display_number, name,
                    description, open_date, status);
        } else {
            displayMessage = getActivity().getString(R.string.dialog_data_not_found);
        }
        alertDialogBuilder.setMessage(Html.fromHtml(displayMessage));
        // set positive button: Ok message
        alertDialogBuilder.setPositiveButton(R.string.dialog_okay_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Track.i(TAG, "Okay Clicked on Dialog");
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @NonNull
    private String checkForNullity(String text) {
        if (TextUtils.isEmpty(text)) {
            text = "";
        }
        return text;
    }


}
