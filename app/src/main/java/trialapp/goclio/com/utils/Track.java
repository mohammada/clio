package trialapp.goclio.com.utils;

import android.util.Log;

import trialapp.goclio.com.BuildConfig;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 * <p/>
 * This class facilitates logging and will take pressure off processor when not debugging
 */

public class Track {

    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg);
        }
    }


    public static void v(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg, t);
        }
    }


    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }


    public static void d(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg, t);
        }
    }


    public static void i(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg);
        }
    }


    public static void i(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg, t);
        }
    }


    public static void w(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, msg);
        }
    }


    public static void w(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, msg, t);
        }
    }


    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg);
        }
    }


    public static void e(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg, t);
        }


    }


    public static void wtf(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.wtf(tag, msg);
        }
    }


    public static void wtf(String tag, String msg, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.wtf(tag, msg, t);
        }
    }
}
