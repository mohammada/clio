package trialapp.goclio.com.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 * <p/>
 * Class responsible to store text on sharedPref
 */

public class SharedPref {
    private static final String TAG = SharedPref.class.getSimpleName();

    public static final String PREF_ALL_MATTER_DATA = "all_matter_data";
    public static final String PREF_MATTER_STORED = "is_matter_stored";

    public static void savePreferences(Context context, String matterData) {
        Track.i(TAG, "Matter Data being saved to Shared Preferences");
        getSharedPreferences(context).edit().putString(PREF_ALL_MATTER_DATA, matterData).commit();
        getSharedPreferences(context).edit().putBoolean(PREF_MATTER_STORED, true).commit();
    }

    public static Boolean isDataStored(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_MATTER_STORED, false);
    }

    public static String getMatterData(Context context) {
        Track.i(TAG, "Matter Data being loaded from to Shared Preferences");
        return getSharedPreferences(context).getString(PREF_ALL_MATTER_DATA, "");
    }

    public static void wipeMatterData(Context context) {
        Track.i(TAG, "Wiping Matter Data");
        getSharedPreferences(context).edit().putString(PREF_ALL_MATTER_DATA, "").commit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context);
    }
}
