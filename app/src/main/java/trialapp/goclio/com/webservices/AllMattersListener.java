package trialapp.goclio.com.webservices;

import trialapp.goclio.com.webservices.models.Matters;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 */

public interface AllMattersListener {

    void onMattersDownloadSuccess(Matters allMattersModel);

    void onMattersDownloadFailed(String errorMessage, int statusCode);
}
