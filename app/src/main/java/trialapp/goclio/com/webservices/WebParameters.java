package trialapp.goclio.com.webservices;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 */

public interface WebParameters {

    String ACCEPTS = "accepts";
    String CONTENTTYPE = "Content-Type";
    String AUTHORIZATION = "Authorization";


    String APPJSON = "application/json";

}
