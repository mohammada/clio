package trialapp.goclio.com.webservices.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 */

public class Matters {

    public List<MattersSubclass> matters;

    public int records;
    public int limit;
    public int next_offset;
    public String order_dir;
    public int total_records;
    public String published_at;

    public class MattersSubclass {

        public int id;
        public Client client;

        public class Client {
            public int id;
            public String url;
            public String name;
        }

        public String display_number;
        public String description;
        public String status;
        public String created_at;
        public String updated_at;

        // Some of the model variables are skipped since they won't be needed for this MVP
        public String open_date;
        public String close_date;
        public String pending_date;
        public String location;
        public String client_reference;
        public String responsible_attorney;
        public String originating_attorney;
        public String practice_area;
        public boolean billable;
        public String maildrop_address;
        public String billing_method;
        public int group_id;
    }

    public ListMatters getListMatters() {
        ListMatters listMatters = new ListMatters();

        ArrayList<Integer> idCollection = new ArrayList<>();
        ArrayList<String> displayNameCollection = new ArrayList<>();
        ArrayList<String> dateCreatedCollection = new ArrayList<>();
        ArrayList<String> dateUpdatedCollection = new ArrayList<>();
        if (matters != null) {
            for (MattersSubclass mattersSubclass : matters) {
                idCollection.add(mattersSubclass.client.id);
                displayNameCollection.add(mattersSubclass.display_number);
                dateCreatedCollection.add(mattersSubclass.created_at);
                dateUpdatedCollection.add(mattersSubclass.updated_at);
            }
        }

        listMatters.setIdCollection(idCollection);
        listMatters.setDisplayNameCollection(displayNameCollection);
        listMatters.setDateCreatedCollection(dateCreatedCollection);
        listMatters.setDateUpdatedCollection(dateUpdatedCollection);

        return listMatters;
    }

}
