package trialapp.goclio.com.webservices.models;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 * <p/>
 * A wrapper class to hold row item values for the List of Matters
 */

public class ListMatters {

    // todo make this String come from String Resources
    public static final String CREATED_D_DAYS_AGO = "Created %d days ago";
    public static final String SOME_DAYS_AGO = "Some days ago";
    public static final String DISPLAY_NAME_MISSING = "Display Name Missing";
    public static final String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";


    public ArrayList<String> DisplayNumList;
    public ArrayList<String> CreatedTimeTextList;
    public ArrayList<String> UpdatedTimeTextList;
    public ArrayList<Integer> MatterIdList;


    public void setIdCollection(ArrayList<Integer> idCollection) {
        ArrayList<Integer> idCollectionList = new ArrayList<>();
        // Checking for nullity and avoiding crashes
        for (Integer id : idCollection) {
            if (id != null) {
                idCollectionList.add(id);
            } else {
                idCollectionList.add(0);
            }
        }

        this.MatterIdList = idCollectionList;
    }

    public void setDisplayNameCollection(ArrayList<String> displayNameCollection) {
        ArrayList<String> displayNameList = new ArrayList<>();
        // Checking for nullity and avoiding crashes
        for (String displayName : displayNameCollection) {
            if (!TextUtils.isEmpty(displayName)) {
                displayNameList.add(displayName);
            } else {
                displayNameList.add(DISPLAY_NAME_MISSING);
            }
        }

        this.DisplayNumList = displayNameList;
    }

    public void setDateCreatedCollection(ArrayList<String> dateCreatedCollection) {
        ArrayList<String> createdTimeText = new ArrayList<>();
        long days;
        for (String datecreated : dateCreatedCollection) {
            if (!TextUtils.isEmpty(datecreated)) {
                days = getDays(datecreated);
                createdTimeText.add(String.format(CREATED_D_DAYS_AGO, days));
            } else {
                // Fill in text if date is empty or not defined
                createdTimeText.add(SOME_DAYS_AGO);
            }
        }
        this.CreatedTimeTextList = createdTimeText;
    }

    public void setDateUpdatedCollection(ArrayList<String> dateUpdatedCollection) {
        ArrayList<String> updatedTimeText = new ArrayList<>();
        long days;
        for (String dateCreated : dateUpdatedCollection) {
            if (!TextUtils.isEmpty(dateCreated)) {
                days = getDays(dateCreated);
                updatedTimeText.add(String.format(CREATED_D_DAYS_AGO, days));
            } else {
                // Fill in text if date is empty or not defined
                updatedTimeText.add(SOME_DAYS_AGO);
            }
        }
        this.UpdatedTimeTextList = updatedTimeText;
    }

    // Get number of days from today date
    // todo This whole method should be moved to a separate class under utils
    private long getDays(String dateValueText) {
        SimpleDateFormat format = new SimpleDateFormat(YYYY_MM_DD_T_HH_MM_SS, Locale.US);
        Date dateCreated = null;
        try {
            // I totally regret spending time parsing date and time but here is a workaround
            // to fix the timezone so the string text coming from the server can get parsed
            dateCreated = format.parse(dateValueText.substring(0, dateValueText.length() - 6));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar dateNow = Calendar.getInstance();
        long diff = 0;
        if (dateCreated != null) {
            diff = dateNow.getTime().getTime() - dateCreated.getTime();
        }
        return TimeUnit.MILLISECONDS.toDays(diff);
    }
}
