package trialapp.goclio.com.webservices;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import trialapp.goclio.com.BuildConfig;
import trialapp.goclio.com.webservices.models.Matters;

/**
 * Created by Mohammad on 8/27/2015.
 * email: mohammad.sfu@gmail.com
 * linkedIn: http://www.linkedin.com/in/mohammadak
 * <p/>
 * this singleton class is responsible for any connection to the server
 */

public class NetworkManager {
    private static final String TAG = NetworkManager.class.getSimpleName();

    private static NetworkManager sInstance;
    private Context mAppContext;
    private RequestQueue mRequestQueue;
    private Object dataConnectionTag = new Object();

    private NetworkManager(Context context) {
        if (mAppContext == null || mRequestQueue == null) {
            mAppContext = context;
            mRequestQueue = Volley.newRequestQueue(context);
        }
    }

    public static NetworkManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new NetworkManager(context.getApplicationContext());
        }
        return sInstance;
    }

    public void getAllMattersFromServer(Object tag, final AllMattersListener allMattersListener) {

        final WeakReference<AllMattersListener> allMattersListenerWeakReference = new WeakReference<AllMattersListener>(allMattersListener);

        // setup http headers
        HashMap<String, String> headerParams = new HashMap<String, String>();
        headerParams.put(WebParameters.AUTHORIZATION, BuildConfig.authorization_key);
        headerParams.put(WebParameters.ACCEPTS, WebParameters.APPJSON);
        headerParams.put(WebParameters.CONTENTTYPE, WebParameters.APPJSON);

        // Create the volley request to access the server
        GsonRequest<Matters> serverMatterDataRequest = new GsonRequest<Matters>(Request.Method.GET, BuildConfig.all_matter_url,
                Matters.class, headerParams, null, new Response.Listener<Matters>() {
            @Override
            public void onResponse(Matters response) {
                AllMattersListener allMattersListener1 = allMattersListenerWeakReference.get();
                if (response != null) {
                    allMattersListener1.onMattersDownloadSuccess(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AllMattersListener allMattersListener1 = allMattersListenerWeakReference.get();
                int statusCode;
                if (error.networkResponse != null) {
                    statusCode = error.networkResponse.statusCode;
                    allMattersListener1.onMattersDownloadFailed(error.getMessage(), statusCode);
                }
            }
        });

        // Set the tag for the request so that it can be cancelled later.
        serverMatterDataRequest.setTag(tag);
        // Add the request to the request queue.
        mRequestQueue.add(serverMatterDataRequest);
    }
}
